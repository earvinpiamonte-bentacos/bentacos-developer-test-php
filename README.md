# Bentacos Developer test - PHP

## Problem

We want to distribute all the colors in 24 hours but there are only 6 variation of colors. This means that each variation of colors should only be used 4 times.

## Objective

Given an array of colors, find the `index` of the color base on the current hour (24 hour format) in Philippine Standart Time.

## Examples

If the current hour is `1`, then the index on the array should be `0`.

If the current hour is `2`, then the index on the array should be `0`.

If the current hour is `5`, then the index on the array should be `1`.

If the current hour is `24`, then the index on the array should be `5`.

![picture](code-colors-mapping.jpg)

## Output

Modify the code to get the correct index of the color array base on the current hour.

## Hot tip:

- There can be no `if` or `else`, statements to answer this test.
- Do not hesitate to ask questions
- You can do it XD

## Submission

After modifying the code, copress the whole project using
compression tools like WinRAR or 7-Zip. The file name of the compressed project should your first name and last name separated by dash.

eg.

```
Juan-Dela-Cruz.zip
```

The subject should be the following `Bentacos Developer Test`.

Email the project to [support@bentacos.com](mailto:support@bentacos.com).

## About

This test was created by Noel Earvin Piamonte on December 02, 2019.
