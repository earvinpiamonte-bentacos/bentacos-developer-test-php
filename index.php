<?php

date_default_timezone_set('Asia/Manila');

$colors = [
  [
    'bg_color'   => '#3498db',
    'text_color' => '#000',
  ],
  [
    'bg_color'   => '#f1c40f',
    'text_color' => '#000',
  ],
  [
    'bg_color'   => '#f39c12',
    'text_color' => '#000',
  ],
  [
    'bg_color'   => '#e67e22',
    'text_color' => '#000',
  ],
  [
    'bg_color'   => '#2c3e50',
    'text_color' => '#000',
  ],
  [
    'bg_color'   => '#000',
    'text_color' => '#fff',
  ],
];

// Make the $index dynamic. Please refer to README.md

$index = 0;

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>Bentacos OJT test</title>

  <style>
    * {
      box-sizing: border-box;
      padding: 0px;
      margin: 0px;
    }
    html,
    body {
      height: 100%;
    }
    body {
      font-family: "Courier New";
    }
    h1 {
      text-transform: uppercase;
      text-align: center;
      color: #fff;
    }
    section {
      display: flex;
      align-items: center;
      justify-content: center;
      height: 100%;
      background-color: #fff;
    }

  </style>

</head>
<body>

<section style="background-color: <?php echo $colors[$index]['bg_color']; ?>">
    <h1 style="color: <?php echo $colors[$index]['text_color'] ?>">Hello, World</h1>
</section>

</body>
</html>